package Answer4;

public class Main {

	public static void main(String[] args) {
		try{
			MyClass c = new MyClass();
			System.out.print("A");
			c.methX();
			System.out.print("B");
			c.methY();
			System.out.print("C");
			return;	
		}
		catch (DataException e){
			System.out.print("D");
		}
		catch (FormatException e){
			System.out.print("E");
		}
		finally{
			System.out.print("F");
		}
		System.out.print("G");
	}
}
/*
	2. �ҡ����� Exception �鴹��о�����ͤ�����͡�ҧ˹�Ҩ� 
		A B C F 
	3. �ҡ methX() �¹ DataException �鴹��о�����ͤ�����͡�ҧ˹�Ҩ�
		A D F G
	4. �ҡ����ա���¹ Exception �� methX() ���ա���¹ FormatException � methY() 
	�鴹��о�����ͤ�����͡�ҧ˹�Ҩ�
		A B E F G
*/
