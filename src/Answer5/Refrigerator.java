package Answer5;

import java.util.ArrayList;

public class Refrigerator {
	private int size;
	private ArrayList<String> things;
	
	public Refrigerator(int size){
		this.size = size;
		things = new ArrayList<String>();
	}
	
	public void put(String stuff) throws FullException {
		if(things.size() == size){
			throw new FullException();
		}
		else{
			things.add(stuff);
		}
	}
	
	public String takeOut(String stuff){
		if(things.contains(stuff)){
			return stuff;
		}
		else{
			return null;
			
		}
	}

	public String toString(){
		String str = "";
		for(String i:things){
			str = str+i+" ";
		}
		return str;
	}
}
