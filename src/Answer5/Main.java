package Answer5;

public class Main {

	public static void main(String[] args) {
		Refrigerator ref = new Refrigerator(4);
		System.out.println("Before: "+ref.toString());
		try {
			ref.put("ice-cream");
			ref.put("coke");
			ref.put("yogurt");
			ref.put("apple");
			ref.put("cake");
			System.out.println(ref.takeOut("coke"));
			System.out.println(ref.takeOut("apple"));
		}
		catch (FullException e) {
			System.err.println("Refrigerator is already full.");
		}
		finally{
			System.out.println("");
			System.out.println("After: "+ref.toString());
		}

	}

}
